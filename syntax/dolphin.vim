if exists("b:current_syntax")
  finish
endif

syn keyword Boolean true false
syn keyword Conditional if else
syn keyword Repeat for while 
syn keyword TypeDef record
syn keyword Keyword var let break continue return new length_of
syn keyword Type void bool int byte string

syn match Type '\(:\)\@<=\s*\(\w\|\d\|\[\|\]\)\+'
syn match Type '^\w\+\(\s\+\(\w\|\d\)\+\s*(\)\@='
syn match Function '\w\+\s*\((\)\@='

syn match Comment '//.\+'
syn region Comment start='/\*' end='\*/'

syn match Number '\(\w\)\@<![\-+]\?\d\+\(\.\d*\)\?'
syn region String start='"' end='"'

let b:current_syntax = "dolphin"
